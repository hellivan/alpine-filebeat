FROM alpine:3.4

ENV FILEBEAT_VERSION=v5.0.0-beta1

RUN apk update &&\
    apk add git make go bash &&\
    cd /tmp &&\
    git clone https://github.com/elastic/beats.git beats &&\
    mkdir /tmp/beats/filebeat/GOPATH &&\
    export GOPATH=/tmp/beats/filebeat/GOPATH &&\
    cd beats &&\
    git checkout ${FILEBEAT_VERSION} &&\
    cd filebeat &&\
    go get; make &&\
    cp ./filebeat /usr/local/bin/filebeat &&\
    cp ./filebeat.yml /etc/filebeat/config.yml &&\
    cd / &&\
    rm -rf /tmp/beats &&\
    akp del git make go bash


ENTRYPOINT ["/usr/local/bin/filebeat"]

CMD ["-e", "-c /etc/filebeat/config.yml"]
    
    
    